package com.hackflyer.hackflyer;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;


public class TagFragment extends Fragment {

    private static View view;

    public static TagFragment newInstance() {
        TagFragment fragment = new TagFragment();
        return fragment;
    }

    public TagFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_tags, container, false);
            for (String tag : SelectActionActivity.tags) {
                CheckBox checkBox = new CheckBox(getActivity());
                checkBox.setText(tag);
                ((LinearLayout) view.findViewById(R.id.tagList)).addView(checkBox);
            }
        } catch (InflateException e) {
            // should not happen
        }
        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
