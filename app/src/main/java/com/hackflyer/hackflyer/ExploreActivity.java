package com.hackflyer.hackflyer;

import android.support.v4.app.FragmentTabHost;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

public class ExploreActivity extends AbstractActionBarActivity {

    private FragmentTabHost mTabHost;

    @Override
    protected void doSetContentView() {
        setContentView(R.layout.activity_explore);

        mTabHost = (FragmentTabHost) findViewById(R.id.tabHost);
        mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);

        mTabHost.addTab(mTabHost.newTabSpec("explore").setIndicator("Explore"),
                ExploreFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("list").setIndicator("List"),
                TagFragment.class, null);
    }

    @Override
    protected void addCustomButton(ViewGroup actionBarLayout) {
        Button settingsButton = new Button(this);
        settingsButton.setText("Settings");

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.LEFT_OF, R.id.authButton);
        params.rightMargin = 20;
        settingsButton.setLayoutParams(params);

        actionBarLayout.addView(settingsButton);
    }
}
