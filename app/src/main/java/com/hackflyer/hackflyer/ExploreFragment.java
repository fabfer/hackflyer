package com.hackflyer.hackflyer;

import android.app.Activity;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class ExploreFragment extends Fragment {

    private static View view;

    private GoogleMap gm;
    private FragmentManager fm;

    private Circle mCircle;
    private Marker mMarker;

    private int radiusInMeters;

    public static ExploreFragment newInstance() {
        ExploreFragment fragment = new ExploreFragment();
        return fragment;
    }

    public ExploreFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fm = getChildFragmentManager();
        radiusInMeters = 25000; // TODO: get radius from server
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_explore, container, false);

            SupportMapFragment mf = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.googleMapSupport);
            gm = mf.getMap();

            gm.setMyLocationEnabled(true);
            gm.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {
                    LatLng currentPosition = new LatLng(location.getLatitude(), location.getLongitude());
                    if (mCircle == null || mMarker == null) {
                        drawMarkerWithCircle(currentPosition);
                        CameraUpdate center =
                                CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
                        gm.moveCamera(center);
                        gm.animateCamera(CameraUpdateFactory.zoomTo((float) (21 - 12.5f * Math.sqrt(Math.sqrt(Math.sqrt(radiusInMeters / 100000.0f))))));
                    } else {
                        updateMarkerWithCircle(currentPosition);
                    }
                }
            });

        } catch (InflateException e) {
            // do nothing, assuming that this does not happen :)
        }
        return view;
    }

    private void updateMarkerWithCircle(LatLng position) {
        if (mCircle != null && mMarker != null) {
            mCircle.setCenter(position);
            mCircle.setRadius(radiusInMeters);
            mMarker.setPosition(position);
        }
    }

    private void drawMarkerWithCircle(LatLng position) {
        int strokeColor = Color.argb(255, 0, 0, 255);

        CircleOptions circleOptions = new CircleOptions().center(position).radius(radiusInMeters).strokeColor(strokeColor).strokeWidth(8);
        mCircle = gm.addCircle(circleOptions);

        MarkerOptions markerOptions = new MarkerOptions().position(position);
        mMarker = gm.addMarker(markerOptions);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
