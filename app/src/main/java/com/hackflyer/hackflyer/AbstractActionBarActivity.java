package com.hackflyer.hackflyer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.AppEventsLogger;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

import java.util.Arrays;

/**
 * Created by fabianferihumer on 21.03.15.
 */
public abstract class AbstractActionBarActivity extends ActionBarActivity {

    private static final String TAG = "ABSTRACT_ACTIVITY";

    private UiLifecycleHelper uiHelper;
    private LoginButton loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);

        doSetContentView();

        if (!showActionBar()) { // suppose that this is login page
            loginButton = (LoginButton) findViewById(R.id.authButton);
            loginButton.setReadPermissions(Arrays.asList("public_profile"));
            loginButton.setSessionStatusCallback(callback);
        }
    }

    protected abstract void doSetContentView();

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
        if (state.isOpened()) {
            Log.i(TAG, "Logged in...");
            Request request = Request.newMeRequest(session, new Request.GraphUserCallback() {
                @Override
                public void onCompleted(GraphUser user, Response response) {
                    // If the response is successful
                    if (session == Session.getActiveSession()) {
                        if (user != null) {
                            String userID = user.getId();//user id
                            String profileName = user.getName();//user's profile name
                            onLogin(userID, profileName);
                        }
                    }
                }
            });
            Request.executeBatchAsync(request);
        } else if (state.isClosed()) {
            Log.i(TAG, "Logged out...");
            onLogout();
        }
    }

    /** Basic implementations, assumed to be logged in **/
    protected void onLogout() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    protected void onLogin(String userID, String profileName) {
        // nothing to to, already logged in
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (showActionBar()) {
            final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                    R.layout.hack_flyer_action_bar,
                    null);

            // Set up your ActionBar
            final ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(actionBarLayout);

            loginButton = (LoginButton) actionBarLayout.findViewById(R.id.authButton);
            loginButton.setSessionStatusCallback(callback);
            
            addCustomButton(actionBarLayout);

            TextView title = (TextView) actionBarLayout.findViewById(R.id.title);
            title.setText(getTitle());
            return true;
        } else {
            getSupportActionBar().hide();
        }

        return false;
    }

    protected void addCustomButton(ViewGroup actionBarLayout) {

    }

    protected boolean showActionBar() {
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();

        // Main activity is launched and user session is not null,
        // but the session state change notification not be triggered.
        Session session = Session.getActiveSession();
        if (session != null &&
                (session.isOpened() || session.isClosed())) {
            onSessionStateChange(session, session.getState(), null);
        }
        AppEventsLogger.activateApp(this);
        uiHelper.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

}
