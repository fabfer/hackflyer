package com.hackflyer.hackflyer;

import android.content.Intent;
import android.view.View;

import com.hackflyer.hackflyer.json.JSONObjectHandler;
import com.hackflyer.hackflyer.json.JSONRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SelectActionActivity extends AbstractActionBarActivity implements JSONObjectHandler {

    public static List<String> tags = new ArrayList<>();

    public void getSearchForFlyerAct(View view) {
        // TODO: change intent, only for testing
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void createFlyer(View view) {
        Intent intent = new Intent(this, CreateFlyerActivity.class);
        startActivity(intent);
    }

    @Override
    protected void doSetContentView() {
        setContentView(R.layout.activity_select_action);
        new JSONRequest(this).execute("http://10.12.172.10/whatsup/gettags.php");
    }

    @Override
    public void handleJSONObject(JSONObject jsonObject) {
        if (jsonObject != null) {
            try {
                JSONArray tagArray = jsonObject.getJSONArray("tags");
                for (int i = 0; i < tagArray.length(); i++) {
                    String tag = tagArray.getString(i);
                    tags.add(tag);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
