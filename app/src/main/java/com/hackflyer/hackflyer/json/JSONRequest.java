package com.hackflyer.hackflyer.json;

import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by fabianferihumer on 22.03.15.
 */
public class JSONRequest extends AsyncTask<String, Void, JSONObject> {

    private JSONObjectHandler jsonObjectHandler;

    public JSONRequest(JSONObjectHandler jsonObjectHandler) {
        this.jsonObjectHandler = jsonObjectHandler;
    }

    @Override
    protected JSONObject doInBackground(String... params) {
        HttpGet get = new HttpGet(params[0]);
        StringBuilder sb = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        JSONObject jo = null;

        try {
            HttpResponse resp = client.execute(get);
            StatusLine sl = resp.getStatusLine();
            int code = sl.getStatusCode();

            if (code == 200) {
                HttpEntity entity = resp.getEntity();
                InputStream is = entity.getContent();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));

                String currentLine;
                while ((currentLine = br.readLine()) != null) {
                    sb.append(currentLine);
                }
            }
            jo = new JSONObject(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jo;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        jsonObjectHandler.handleJSONObject(jsonObject);
    }

}