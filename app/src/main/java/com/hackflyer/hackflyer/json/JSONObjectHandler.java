package com.hackflyer.hackflyer.json;

import org.json.JSONObject;

/**
 * Created by fabianferihumer on 22.03.15.
 */
public interface JSONObjectHandler {

    public void handleJSONObject(JSONObject jsonObject);

}
