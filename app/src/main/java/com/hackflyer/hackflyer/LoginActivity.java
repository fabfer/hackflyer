package com.hackflyer.hackflyer;

import android.content.Intent;
import android.widget.Toast;

public class LoginActivity extends AbstractActionBarActivity {

    @Override
    protected void doSetContentView() {
        setContentView(R.layout.activity_login);
    }

    @Override
    protected void onLogout() {
        // nothing to do
    }

    @Override
    protected void onLogin(String userID, String profileName) {
        Toast.makeText(this, userID + " " + profileName, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, SelectActionActivity.class);
        startActivity(intent);
    }

    @Override
    protected boolean showActionBar() {
        return false;
    }

}
