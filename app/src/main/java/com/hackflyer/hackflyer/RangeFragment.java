package com.hackflyer.hackflyer;

import android.app.Activity;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class RangeFragment extends Fragment {

    private static View view;

    private Circle mCircle;
    private Marker mMarker;
    private LatLng currentPosition;
    private int currentRadius;
    private SeekBar seekBar;
    private TextView currentRadiusText;

    public GoogleMap gm;

    public static RangeFragment newInstance() {
        RangeFragment fragment = new RangeFragment();
        return fragment;
    }

    public RangeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_range, container, false);

            SupportMapFragment mf = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.googleMapSupport);
            gm = mf.getMap();

            gm.setMyLocationEnabled(true);
            gm.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {
                    currentPosition = new LatLng(location.getLatitude(), location.getLongitude());
                    if (mCircle == null || mMarker == null) {
                        drawMarkerWithCircle(currentPosition);
                        CameraUpdate center =
                                CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
                        gm.moveCamera(center);
                        updateZoom();
                    } else {
                        updateMarkerWithCircle(currentPosition);
                    }
                }
            });

            seekBar = (SeekBar) view.findViewById(R.id.radiusSeekBar);
            currentRadiusText = (TextView) view.findViewById(R.id.currentRadius);
            currentRadius = seekBar.getProgress();
            // TODO: try to load saved radius if available!!

            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    currentRadius = progress;
                    currentRadiusText.setText((progress + 1) + " km");
                    updateZoom();
                    updateMarkerWithCircle(currentPosition);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        } catch (InflateException e) {
            // should not happen
        }
        // Inflate the layout for this fragment
        return view;
    }

    private void updateZoom() {
        gm.animateCamera(CameraUpdateFactory.zoomTo((float) (21 - 12.5f * Math.sqrt(Math.sqrt(Math.sqrt((currentRadius + 1) / (float) (seekBar.getMax() + 1)))))));
    }

    private void updateMarkerWithCircle(LatLng position) {
        if (mCircle != null && mMarker != null) {
            int radiusInMeters = (seekBar.getProgress() + 1) * 1000;
            mCircle.setCenter(position);
            mCircle.setRadius(radiusInMeters);
            mMarker.setPosition(position);
        }
    }

    private void drawMarkerWithCircle(LatLng position) {
        int radiusInMeters = (seekBar.getProgress() + 1) * 1000;
        int strokeColor = Color.argb(255, 0, 0, 255);

        CircleOptions circleOptions = new CircleOptions().center(position).radius(radiusInMeters).strokeColor(strokeColor).strokeWidth(8);
        mCircle = gm.addCircle(circleOptions);

        MarkerOptions markerOptions = new MarkerOptions().position(position);
        mMarker = gm.addMarker(markerOptions);
    }

    public void saveRadius(View v) {
        // TODO: save current radius to firebase
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
