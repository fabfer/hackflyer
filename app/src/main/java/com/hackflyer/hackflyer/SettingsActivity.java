package com.hackflyer.hackflyer;

import android.support.v4.app.FragmentTabHost;


public class SettingsActivity extends AbstractActionBarActivity {

    private FragmentTabHost mTabHost;

    @Override
    protected void doSetContentView() {
        setContentView(R.layout.activity_settings);

        mTabHost = (FragmentTabHost) findViewById(R.id.tabHost);
        mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);

        mTabHost.addTab(mTabHost.newTabSpec("tags").setIndicator("Tags"),
                TagFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("range").setIndicator("Range"),
                RangeFragment.class, null);
    }

}
